Description
============
This tool is an interpreter for the Attack Description Language (aka ADL) that can be used by an user to describe cyber-physical attacks against cyber-physical systems.
The interpreter parses the ADL file (containing the attacks) and produces the Attack Configuration
File (aka ACF), i.e. an XML representation of the ADL file that is understandable by ASF++.


Requirements
============
This tool requires the library "argparse".

It is also necessary to have PLY (Python Lex-Yacc), created by David Beazley. You can download it from 
his personal [page] (http://www.dabeaz.com/ply/) or from his [Github page](https://github.com/dabeaz/ply).


Usage
=====
``` sh
$ python path/interpreter.py -i inputfile -o outputfile
```
-i inputfile is mandatory

-o outputfile is optional

The commands above produces the XML Attack Configuration File (aka ACF) for ASF++, according to the ADL description in the input file.
Only the input file is mandatory. If the user does not specify the output filename, the interpreter will use the default filename 'file_input.xml'.


Authors
=======

* Francesco Racciatti  	<racciatti.francesco@gmail.com>
* Alessandro Pischedda	<alessandro.pischedda@gmail.com>
* Marco Tiloca		<marco.tiloca84@gmail.com>